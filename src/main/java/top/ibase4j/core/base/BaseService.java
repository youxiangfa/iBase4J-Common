package top.ibase4j.core.base;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.RandomUtils;
import org.apache.ibatis.session.SqlSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.enums.SqlMethod;
import com.baomidou.mybatisplus.exceptions.MybatisPlusException;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.SqlHelper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.toolkit.ReflectionKit;

import top.ibase4j.core.Constants;
import top.ibase4j.core.exception.BusinessException;
import top.ibase4j.core.support.Pagination;
import top.ibase4j.core.support.cache.CacheKey;
import top.ibase4j.core.support.dbcp.HandleDataSource;
import top.ibase4j.core.util.CacheUtil;
import top.ibase4j.core.util.DataUtil;
import top.ibase4j.core.util.ExceptionUtil;
import top.ibase4j.core.util.InstanceUtil;
import top.ibase4j.core.util.PropertiesUtil;

/**
 * 业务逻辑层基类
 *
 * @author ShenHuaJie
 * @version 2016年5月20日 下午3:19:19
 */
public abstract class BaseService<T extends BaseModel, M extends BaseMapper<T>> implements IBaseService<T> {
    protected Logger logger = LogManager.getLogger();
    @Autowired
    protected M mapper;

    int maxThread = PropertiesUtil.getInt("db.reader.list.maxThread", 50);
    int threadSleep = PropertiesUtil.getInt("db.reader.list.threadWait", 5);
    ExecutorService executorService = Executors.newFixedThreadPool(maxThread);

    /** 分页查询参数封装 */
    @SuppressWarnings({"unchecked"})
    public static Page<Long> getPage(Map<String, Object> params) {
        Integer current = 1;
        Integer size = 10;
        String orderBy = "id_", sortAsc = null, openSort = "Y";
        if (DataUtil.isNotEmpty(params.get("pageNumber"))) {
            current = Integer.valueOf(params.get("pageNumber").toString());
        }
        if (DataUtil.isNotEmpty(params.get("pageIndex"))) {
            current = Integer.valueOf(params.get("pageIndex").toString());
        }
        if (DataUtil.isNotEmpty(params.get("pageSize"))) {
            size = Integer.valueOf(params.get("pageSize").toString());
        }
        if (DataUtil.isNotEmpty(params.get("limit"))) {
            size = Integer.valueOf(params.get("limit").toString());
        }
        if (DataUtil.isNotEmpty(params.get("offset"))) {
            current = Integer.valueOf(params.get("offset").toString()) / size + 1;
        }
        if (DataUtil.isNotEmpty(params.get("sort"))) {
            orderBy = (String)params.get("sort");
            params.remove("sort");
        }
        if (DataUtil.isNotEmpty(params.get("orderBy"))) {
            orderBy = (String)params.get("orderBy");
            params.remove("orderBy");
        }
        if (DataUtil.isNotEmpty(params.get("sortAsc"))) {
            sortAsc = (String)params.get("sortAsc");
            params.remove("sortAsc");
        }
        if (DataUtil.isNotEmpty(params.get("openSort"))) {
            openSort = (String)params.get("openSort");
            params.remove("openSort");
        }
        Object filter = params.get("filter");
        if (filter != null) {
            params.putAll(JSON.parseObject(filter.toString(), Map.class));
        }
        if (size == -1) {
            Page<Long> page = new Page<Long>();
            page.setOrderByField(orderBy);
            page.setAsc("Y".equals(sortAsc));
            page.setOpenSort("Y".equals(openSort));
            return page;
        }
        Page<Long> page = new Page<Long>(current, size, orderBy);
        page.setAsc("Y".equals(sortAsc));
        page.setOpenSort("Y".equals(openSort));
        return page;
    }

    /** 逻辑批量删除 */
    @Override
    @Transactional
    public void del(List<Long> ids, Long userId) {
        for (Long id : ids) {
            del(id, userId);
        }
    }

    /** 逻辑删除 */
    @Override
    @Transactional
    public void del(Long id, Long userId) {
        try {
            T record = this.getById(id);
            record.setEnable(0);
            record.setUpdateTime(new Date());
            record.setUpdateBy(userId);
            mapper.updateById(record);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    /** 物理删除 */
    @Override
    @Transactional
    public void delete(Long id) {
        try {
            mapper.deleteById(id);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    /** 物理删除 */
    @Override
    @Transactional
    public Integer deleteByEntity(T t) {
        Wrapper<T> wrapper = new EntityWrapper<T>(t);
        return mapper.delete(wrapper);
    }

    /** 物理删除 */
    @Override
    @Transactional
    public Integer deleteByMap(Map<String, Object> columnMap) {
        return mapper.deleteByMap(columnMap);
    }

    /** 根据Id查询(默认类型Map)  */
    public Pagination<Map<String, Object>> getPageMap(final Page<Long> ids) {
        if (ids != null) {
            Pagination<Map<String, Object>> page = new Pagination<Map<String, Object>>(ids.getCurrent(), ids.getSize());
            page.setTotal(ids.getTotal());
            final List<Map<String, Object>> records = InstanceUtil.newArrayList();
            for (int i = 0; i < ids.getRecords().size(); i++) {
                records.add(null);
            }
            final Map<Integer, Object> thread = InstanceUtil.newConcurrentHashMap();
            final String datasource = HandleDataSource.getDataSource();
            for (int i = 0; i < ids.getRecords().size(); i++) {
                final int index = i;
                executorService.execute(new Runnable() {
                    @Override
                    public void run() {
                        HandleDataSource.putDataSource(datasource);
                        try {
                            records.set(index, InstanceUtil.transBean2Map(getById(ids.getRecords().get(index))));
                        } finally {
                            thread.put(index, 0);
                        }
                    }
                });
            }
            while (thread.size() < records.size()) {
                try {
                    Thread.sleep(threadSleep);
                } catch (InterruptedException e) {
                    logger.error("", e);
                }
            }
            page.setRecords(records);
            return page;
        }
        return new Pagination<Map<String, Object>>();
    }

    /** 根据参数分页查询 */
    @Override
    public Pagination<T> query(Map<String, Object> params) {
        Page<Long> page = getPage(params);
        page.setRecords(mapper.selectIdPage(page, params));
        return getPage(page);
    }

    /** 根据实体参数分页查询 */
    @Override
    public Pagination<T> query(T entity, Pagination<T> rowBounds) {
        Page<T> page = new Page<T>();
        try {
            PropertyUtils.copyProperties(page, rowBounds);
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            logger.error("", e);
        }
        Wrapper<T> wrapper = new EntityWrapper<T>(entity);
        List<T> list = mapper.selectPage(page, wrapper);
        for (T t : list) {
            saveCache(t);
        }
        Pagination<T> pager = new Pagination<T>(page.getCurrent(), page.getSize());
        pager.setRecords(list);
        pager.setTotal(mapper.selectCount(wrapper));
        return pager;
    }

    @Override
    /** 根据id查询实体 */
    public T queryById(Long id) {
        return queryById(id, 1);
    }

    @Override /** 根据参数查询 */
    public List<T> queryList(Map<String, Object> params) {
        if (DataUtil.isEmpty(params.get("orderBy"))) {
            params.put("orderBy", "id_");
        } else if (DataUtil.checkSQLInject(params.get("orderBy").toString())) {
            throw new RuntimeException("非法操作.");
        }
        if (DataUtil.isEmpty(params.get("sortAsc"))) {
            params.put("sortAsc", "desc");
        } else if (DataUtil.checkSQLInject(params.get("sortAsc").toString())) {
            throw new RuntimeException("非法操作.");
        }
        List<Long> ids = mapper.selectIdPage(params);
        List<T> list = queryList(ids);
        return list;
    }

    @Override /** 根据实体参数查询 */
    public List<T> queryList(T params) {
        List<Long> ids = mapper.selectIdPage(params);
        List<T> list = queryList(ids);
        return list;
    }

    /** 根据Id查询(默认类型T)  */
    @Override
    public List<T> queryList(final List<Long> ids) {
        final List<T> list = InstanceUtil.newArrayList();
        if (ids != null) {
            for (int i = 0; i < ids.size(); i++) {
                list.add(null);
            }
            final Map<Integer, Object> thread = InstanceUtil.newConcurrentHashMap();
            final String datasource = HandleDataSource.getDataSource();
            for (int i = 0; i < ids.size(); i++) {
                final int index = i;
                executorService.execute(new Runnable() {
                    @Override
                    public void run() {
                        HandleDataSource.putDataSource(datasource);
                        try {
                            list.set(index, getById(ids.get(index)));
                        } finally {
                            thread.put(index, 0);
                        }
                    }
                });
            }
            while (thread.size() < list.size()) {
                try {
                    Thread.sleep(threadSleep);
                } catch (InterruptedException e) {
                    logger.error("", e);
                }
            }
        }
        return list;
    }

    /** 根据Id查询(cls返回类型Class) */
    @Override
    public <K> List<K> queryList(final List<Long> ids, final Class<K> cls) {
        final List<K> list = InstanceUtil.newArrayList();
        if (ids != null) {
            for (int i = 0; i < ids.size(); i++) {
                list.add(null);
            }
            final Map<Integer, Object> thread = InstanceUtil.newConcurrentHashMap();
            final String datasource = HandleDataSource.getDataSource();
            for (int i = 0; i < ids.size(); i++) {
                final int index = i;
                executorService.execute(new Runnable() {
                    @Override
                    public void run() {
                        HandleDataSource.putDataSource(datasource);
                        try {
                            T t = getById(ids.get(index));
                            K k = InstanceUtil.to(t, cls);
                            list.set(index, k);
                        } finally {
                            thread.put(index, 0);
                        }
                    }
                });
            }
            while (thread.size() < list.size()) {
                try {
                    Thread.sleep(threadSleep);
                } catch (InterruptedException e) {
                    logger.error("", e);
                }
            }
        }
        return list;
    }

    @Override /** 根据实体参数查询一条记录 */
    public T selectOne(T entity) {
        T t = mapper.selectOne(entity);
        saveCache(t);
        return t;
    }

    /** 修改/新增 */
    @Override
    @Transactional
    public T update(T record) {
        try {
            record.setUpdateTime(new Date());
            if (record.getId() == null) {
                record.setCreateTime(new Date());
                mapper.insert(record);
            } else {
                String lockKey = getLockKey("U" + record.getId());
                if (CacheUtil.getLock(lockKey, "更新")) {
                    try {
                        mapper.updateById(record);
                    } finally {
                        CacheUtil.unLock(lockKey);
                    }
                } else {
                    throw new RuntimeException("数据不一致!请刷新页面重新编辑!");
                }
            }
            record = mapper.selectById(record.getId());
            saveCache(record);
        } catch (DuplicateKeyException e) {
            logger.error(Constants.Exception_Head, e);
            throw new BusinessException("已经存在相同的记录.");
        } catch (Exception e) {
            logger.error(Constants.Exception_Head, e);
            throw new RuntimeException(ExceptionUtil.getStackTraceAsString(e));
        }
        return record;
    }

    /** 修改所有字段/新增 */
    @Override
    @Transactional
    public T updateAllColumn(T record) {
        try {
            record.setUpdateTime(new Date());
            if (record.getId() == null) {
                record.setCreateTime(new Date());
                mapper.insert(record);
            } else {
                String lockKey = getLockKey("U" + record.getId());
                if (CacheUtil.getLock(lockKey, "更新所有字段")) {
                    try {
                        mapper.updateAllColumnById(record);
                    } finally {
                        CacheUtil.unLock(lockKey);
                    }
                } else {
                    throw new RuntimeException("数据不一致!请刷新页面重新编辑!");
                }
            }
            record = mapper.selectById(record.getId());
            saveCache(record);
        } catch (DuplicateKeyException e) {
            logger.error(Constants.Exception_Head, e);
            throw new BusinessException("已经存在相同的记录.");
        } catch (Exception e) {
            logger.error(Constants.Exception_Head, e);
            throw new RuntimeException(ExceptionUtil.getStackTraceAsString(e));
        }
        return record;
    }

    /** 批量修改所有字段/新增 */
    @Override
    @Transactional
    public boolean updateAllColumnBatch(List<T> entityList) {
        return updateAllColumnBatch(entityList, 30);
    }

    /** 批量修改所有字段/新增 */
    @Override
    @Transactional
    public boolean updateAllColumnBatch(List<T> entityList, int batchSize) {
        return updateBatch(entityList, batchSize, false);
    }

    /** 批量修改/新增 */
    @Override
    @Transactional
    public boolean updateBatch(List<T> entityList) {
        return updateBatch(entityList, 30);
    }

    /** 批量修改/新增 */
    @Override
    @Transactional
    public boolean updateBatch(List<T> entityList, int batchSize) {
        return updateBatch(entityList, batchSize, true);
    }

    @SuppressWarnings("unchecked")
    protected Class<T> currentModelClass() {
        return ReflectionKit.getSuperClassGenricType(getClass(), 0);
    }

    /**
     * 获取缓存键值
     *
     * @param id
     * @return
     */
    protected String getLockKey(Object id) {
        CacheKey cacheKey = CacheKey.getInstance(getClass());
        StringBuilder sb = new StringBuilder();
        if (cacheKey == null) {
            sb.append(getClass().getName());
        } else {
            sb.append(cacheKey.getValue());
        }
        return sb.append(":LOCK:").append(id).toString();
    }

    /**
     * @param params
     * @param cls
     * @return
     */
    protected <P> Pagination<P> query(Map<String, Object> params, Class<P> cls) {
        Page<Long> page = getPage(params);
        page.setRecords(mapper.selectIdPage(page, params));
        return getPage(page, cls);
    }

    /**
     * @param millis
     */
    protected void sleep(int millis) {
        try {
            Thread.sleep(RandomUtils.nextLong(10, millis));
        } catch (InterruptedException e) {
            logger.error("", e);
        }
    }

    /**
     * <p>
     * 批量操作 SqlSession
     * </p>
     */
    protected SqlSession sqlSessionBatch() {
        return SqlHelper.sqlSessionBatch(currentModelClass());
    }

    /**
     * 获取SqlStatement
     *
     * @param sqlMethod
     * @return
     */
    protected String sqlStatement(SqlMethod sqlMethod) {
        return SqlHelper.table(currentModelClass()).getSqlStatement(sqlMethod.getMethod());
    }

    /** 根据Id查询(默认类型T) */
    private T getById(Long id) {
        return queryById(id, 1);
    }

    /** 根据Id批量查询(默认类型T) */
    protected Pagination<T> getPage(final Page<Long> ids) {
        if (ids != null) {
            Pagination<T> page = new Pagination<T>(ids.getCurrent(), ids.getSize());
            page.setTotal(ids.getTotal());
            final List<T> records = InstanceUtil.newArrayList();
            for (int i = 0; i < ids.getRecords().size(); i++) {
                records.add(null);
            }
            final Map<Integer, Object> thread = InstanceUtil.newConcurrentHashMap();
            final String datasource = HandleDataSource.getDataSource();
            for (int i = 0; i < ids.getRecords().size(); i++) {
                final int index = i;
                executorService.execute(new Runnable() {
                    @Override
                    public void run() {
                        HandleDataSource.putDataSource(datasource);
                        try {
                            records.set(index, getById(ids.getRecords().get(index)));
                        } finally {
                            thread.put(index, 0);
                        }
                    }
                });
            }
            while (thread.size() < records.size()) {
                try {
                    Thread.sleep(threadSleep);
                } catch (InterruptedException e) {
                    logger.error("", e);
                }
            }
            page.setRecords(records);
            return page;
        }
        return new Pagination<T>();
    }

    /** 根据Id查询(cls返回类型Class) */
    private <K> Pagination<K> getPage(final Page<Long> ids, final Class<K> cls) {
        if (ids != null) {
            Pagination<K> page = new Pagination<K>(ids.getCurrent(), ids.getSize());
            page.setTotal(ids.getTotal());
            final List<K> records = InstanceUtil.newArrayList();
            for (int i = 0; i < ids.getRecords().size(); i++) {
                records.add(null);
            }
            final Map<Integer, Object> thread = InstanceUtil.newConcurrentHashMap();
            final String datasource = HandleDataSource.getDataSource();
            for (int i = 0; i < ids.getRecords().size(); i++) {
                final int index = i;
                executorService.execute(new Runnable() {
                    @Override
                    public void run() {
                        HandleDataSource.putDataSource(datasource);
                        try {
                            T t = getById(ids.getRecords().get(index));
                            K k = InstanceUtil.to(t, cls);
                            records.set(index, k);
                        } finally {
                            thread.put(index, 0);
                        }
                    }
                });
            }
            while (thread.size() < records.size()) {
                try {
                    Thread.sleep(threadSleep);
                } catch (InterruptedException e) {
                    logger.error("", e);
                }
            }
            page.setRecords(records);
            return page;
        }
        return new Pagination<K>();
    }

    /** 保存缓存 */
    private void saveCache(T record) {
        if (record == null) {
            return;
        }
        CacheKey key = CacheKey.getInstance(getClass());
        if (key != null) {
            try {
                CacheUtil.getCache().set(key.getValue() + ":" + record.getId(), record, key.getTimeToLive());
            } catch (Exception e) {
                logger.error(Constants.Exception_Head, e);
            }
        }
    }

    @SuppressWarnings("unchecked")
    private T queryById(Long id, int times) {
        CacheKey key = CacheKey.getInstance(getClass());
        T record = null;
        if (key != null) {
            try {
                record = (T)CacheUtil.getCache().get(key.getValue() + ":" + id, key.getTimeToLive());
            } catch (Exception e) {
                logger.error(Constants.Exception_Head, e);
            }
        }
        if (record == null) {
            String lockKey = getLockKey(id);
            if (CacheUtil.getLock(lockKey, "根据ID查询数据")) {
                try {
                    record = mapper.selectById(id);
                    saveCache(record);
                } finally {
                    CacheUtil.unLock(lockKey);
                }
            } else {
                if (times > 3) {
                    record = mapper.selectById(id);
                    saveCache(record);
                } else {
                    logger.debug(getClass().getSimpleName() + ":" + id + " retry getById.");
                    sleep(20);
                    return queryById(id, times + 1);
                }
            }
        }
        return record;
    }

    private boolean updateBatch(List<T> entityList, int batchSize, boolean selective) {
        if (CollectionUtils.isEmpty(entityList)) {
            throw new IllegalArgumentException("Error: entityList must not be empty");
        }
        try (SqlSession batchSqlSession = sqlSessionBatch()) {
            int size = entityList.size();
            for (int i = 0; i < size; i++) {
                if (selective) {
                    update(entityList.get(i));
                } else {
                    updateAllColumn(entityList.get(i));
                }
                if (i >= 1 && i % batchSize == 0) {
                    batchSqlSession.flushStatements();
                }
            }
            batchSqlSession.flushStatements();
        } catch (Throwable e) {
            throw new MybatisPlusException("Error: Cannot execute insertOrUpdateBatch Method. Cause", e);
        }
        return true;
    }
}
