package top.ibase4j.core.base;

import java.util.List;
import java.util.Map;

import org.springframework.transaction.annotation.Transactional;

import top.ibase4j.core.support.Pagination;

/**
 * @author ShenHuaJie
 * @version 2016年5月20日 下午3:19:19
 */
public interface IBaseService<T extends BaseModel> {
    @Transactional
    public T update(T record);

    @Transactional
    public void del(List<Long> ids, Long userId);

    @Transactional
    public void del(Long id, Long userId);

    @Transactional
    public void delete(Long id);

    @Transactional
    public Integer deleteByEntity(T t);

    @Transactional
    public Integer deleteByMap(Map<String, Object> columnMap);

    public T queryById(Long id);

    public Pagination<T> query(Map<String, Object> params);

    public Pagination<T> query(T entity, Pagination<T> rowBounds);

    public List<T> queryList(Map<String, Object> params);

    public List<T> queryList(List<Long> ids);

    public <K> List<K> queryList(List<Long> ids, Class<K> cls);

    public List<T> queryList(T entity);

    public T selectOne(T entity);

    public T updateAllColumn(T record);

    public boolean updateAllColumnBatch(List<T> entityList);

    public boolean updateAllColumnBatch(List<T> entityList, int batchSize);

    public boolean updateBatch(List<T> entityList);

    public boolean updateBatch(List<T> entityList, int batchSize);
}
