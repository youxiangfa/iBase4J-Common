package top.ibase4j.core.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * @author ShenHuaJie
 * @since 2018年4月21日 下午12:49:47
 */
@Deprecated
public class ServerListener extends ApplicationReadyListener implements ServletContextListener {
    public void contextInitialized(ServletContextEvent contextEvent) {
        logger.info("=================================");
        logger.info("系统[{}]启动完成!!!", contextEvent.getServletContext().getServletContextName());
        logger.info("=================================");
    }

    public void contextDestroyed(ServletContextEvent contextEvent) {
    }
}
